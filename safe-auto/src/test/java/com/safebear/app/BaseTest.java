package com.safebear.app;

import com.safebear.app.utils.Utils;
import com.safebear.app.utils.pages.*;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class BaseTest {

    WebDriver driver;
    Utils utility = new Utils();
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;
    FramesPageMainFrame framesPageMainFrame;

    @Before
    public void setup () throws MalformedURLException {
//        driver = new ChromeDriver();
        DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"),capabilities);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        assertTrue (utility.navigateToWebsite(driver));
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        framesPage = new FramesPage(driver);
        userPage = new UserPage(driver);


    }
    @After
    public void tearDown () {

        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }
}
